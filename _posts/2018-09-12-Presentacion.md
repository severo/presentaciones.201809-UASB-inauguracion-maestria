---
layout: presentacion
title: Presentación de la Maestría en gerencia de tecnologías libres
slug: 2018-09-12-presentacion-maestria
description:
  Presentación Sylvain Lesage, seminario Gobernando con soberanía tecnológica,
  presentación de la Maestría en gerencia de tecnologías libres de la
  Universidad Andina Simón Bolivar - UASB.
theme: white
transition: slide
date: 2018-08-31 08:00:00 -0400
---

<section data-markdown>
## Maestría en gerencia de tecnologías libres

Universidad Andina Simón Bolivar

---

Seminario "Gobernando con soberanía tecnológica" Cámara de Senadores

La Paz - 12/09/2018

</section>

<section data-markdown>
## Sylvain Lesage

Coordinador de la Maestría

- MAIL: s.lesage@uasb.edu.bo
- TEL: 71542024
- TWITTER: @MAEGTL_andina

</section>

<section data-markdown>
## Plan

- Objetivo
- Público
- Organización
- Curricula y Profesores
- Inscripción y costo

</section>

<section data-markdown>
## El objetivo es:

Saber planificar, ejecutar y dirigir políticas y proyectos tecnológicos dentro
de una entidad pública o privada, con enfoque en tecnologías libres.

</section>

<section data-markdown>
## Está dirigida a:

- **Planificación, jurídica, administración** ↦ modernizar su forma de hacer
  gestión
- **Sistemas** ↦ elaborar y gestionar políticas y proyectos

</section>

<section data-markdown>
## Tiempos y ubicación

- **duración:** 12 meses + tesis
- **horarios:** 19:00 - 22:00, de lunes a viernes
- **inscripción:** hasta el 12 de octubre, o llenar los 30 cupos
- **ubicación:** presencial, UASB - Miraflores, La Paz

</section>

<section data-markdown>
## Profesores

- **internacionales:** Stéphane Bortzmeyer (Francia), Ramon Ramon (España),
  Mallory Knodel (EEUU), Daniel Viñar (Uruguay)
- **nacionales:** Ariel Condo, Sylvain Lesage, Ernesto Campohermoso, Julia
  Sillo, Fabiola Ramirez, Eliana Quiroz, Karina Medinaceli, Judith Apaza,
  Patricia Cabero, Patricia Lopez, Lia Solis

</section>

<section data-markdown>
## Programa

- Módulo 1 - Recursos tecnológicos
- Módulo 2 - Soberanía tecnológica e innovación
- Módulo 3 - Planificación y ejecución
- Módulo 4 - Tesis

[Descargar el programa](/presentaciones/presentaciones/20180912-UASB-inauguracion-maestria/assets/pdf/MAEGTL.pdf)

</section>

<section data-markdown>
## Inversión

Se cuenta con una subvención estatal, el valor total es de 4.350 dólares:

- 650 dólares de matrícula
- 3.350 dólares de colegiatura
- 350 dólares del título académico

Se puede pagar en cuotas mensuales.

</section>

<section data-markdown>
## Para más información...

... acercarse al personal de la UASB o

escribir al coordinador Sylvain LESAGE:

- s.lesage@uasb.edu.bo
- 71542024

</section>
