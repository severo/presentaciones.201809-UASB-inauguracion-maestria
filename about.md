---
layout: page
title: Más información
permalink: /about/
---

Este sitio contiene las presentaciones y materiales para la inauguración de la
Maestría en gerencia de tecnologías libres de la Universidad Andina Simón
Bolivar - UASB.

Todo el material está publicada bajo licencia
[CC-BY](https://creativecommons.org/licenses/by/4.0/). El código de este sitio,
hecho con
[Jekyll y Reveal.js](http://luugiathuy.com/slides/jekyll-create-slides-with-revealjs/),
se encuentra en
[Framagit](https://framagit.org/severo/presentaciones.201809-UASB-inauguracion-maestria/).

Para ver mis otras presentaciones, ir a
[https://presentaciones.rednegra.net/](https://presentaciones.rednegra.net/).

Para ver mis cursos, ir a
[https://cursos.rednegra.net/](https://cursos.rednegra.net/).

Si estabas buscando información sobre mi, ver
[https://rednegra.net/](https://rednegra.net/sylvainlesage/).
