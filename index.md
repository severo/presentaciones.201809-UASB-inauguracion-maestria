---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# UASB - Presentación de la Maestría en gerencia de tecnologías libres

## Documentos y presentaciones

| Día        | Contenido                                                                |
| ---------- | ------------------------------------------------------------------------ |
| 12/09/2018 | [Presentación]({{ site.baseurl }}{% post_url 2018-09-12-Presentacion %}) |
